import React from 'react';
import {StyleSheet, View, Text, TouchableOpacity, AsyncStorage, Image} from 'react-native';
import getWH from '../constants/Layout';
import themeColor from '../constants/Colors';

const styles = StyleSheet.create({
    container: {
        backgroundColor: themeColor.AyushBackground,
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    headlogo: {
        marginVertical: 10,
        width: getWH.window.width - 60,
    },
    loginbtn: {
        width: getWH.window.width - 60,
        borderColor: themeColor.AyushInputBorderColor,
        backgroundColor: themeColor.AyushButton,
        borderWidth: 1,
        height: 40,
        paddingHorizontal: 10,
        borderRadius: 5,
        marginVertical: 5,
        justifyContent: 'center',
        alignItems: 'center'
    },
    loginbtnText: {
        color: themeColor.AyushButtonText,
        alignItems: 'center',
        fontSize: 18,
        fontWeight: '500'
    }
})


class InitialScreen extends React.Component{
    constructor(props){
        super(props);
        this._bootstrapAsync();
    }

    _bootstrapAsync = async () => {
        const userToken = await AsyncStorage.getItem('userToken');
        this.props.navigation.navigate(userToken ? 'Home' : 'Initial');
      };

    goto_login = () =>{
        this.props.navigation.navigate('Login');
    }

    goto_register = () =>{
        this.props.navigation.navigate('Register');
    }

    logo(){
        const IPHONE7PLUS_WIDTH = 450;
        if (getWH.window.width <= IPHONE7PLUS_WIDTH) {
            return(
            <View style={styles.headlogo}>
                <Image source={require('../assets/images/ayushyaa.png')} style={{width: getWH.window.width - 60, height: 120}} />
            </View>
            )
        }
        else{
            return(
            <View style={styles.headlogo}>
                <Image source={require('../assets/images/ayushyaa.png')} style={{width: getWH.window.width - 60, height: 260}} />
            </View>
            )
        }
    }

    render(){
        return (
            <View style={styles.container}>
                {this.logo()}
                <TouchableOpacity onPress={this.goto_login.bind(this)}>
                    <View style={styles.loginbtn}>
                            <Text style={styles.loginbtnText}>Login</Text>
                    </View>
                </TouchableOpacity>
                <TouchableOpacity onPress={this.goto_register.bind(this)}>
                    <View style={styles.loginbtn}>
                            <Text style={styles.loginbtnText}>New User?</Text>
                    </View>
                </TouchableOpacity>
            </View>
        )
    }
}

export default InitialScreen