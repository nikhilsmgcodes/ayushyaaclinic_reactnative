import React from 'react';
import {StyleSheet, View, TextInput, Text, TouchableOpacity, ToastAndroid, KeyboardAvoidingView, AsyncStorage} from 'react-native';
import getWH from '../constants/Layout';
import themeColor from '../constants/Colors';
import URL from '../constants/Url';
import { Icon } from 'react-native-elements';
import Spinner from 'react-native-loading-spinner-overlay';
import Toast from 'react-native-whc-toast';

const styles = StyleSheet.create({
    container: {
        backgroundColor: themeColor.AyushBackground,
        flex: 1,
        paddingVertical: 10,
        alignItems: 'center'
    },
    clinicName: {
        fontSize: 28,
        fontWeight: '500',
        color: '#ffff00',
        marginVertical: 5
    },
    inputData: {
        width: getWH.window.width - 60,
        borderColor: themeColor.AyushInputBorderColor,
        borderWidth: 1,
        height: 40,
        color: themeColor.AyushInputColor,
        paddingHorizontal: 10,
        borderRadius: 5,
        marginVertical: 5 
    },
    inputIcon: {
        position: 'absolute',
        right: 5,
        top: 12,
    },
    loginbtn: {
        width: getWH.window.width - 60,
        borderColor: themeColor.AyushInputBorderColor,
        backgroundColor: themeColor.AyushButton,
        borderWidth: 1,
        height: 40,
        paddingHorizontal: 10,
        borderRadius: 5,
        marginVertical: 5,
        justifyContent: 'center',
        alignItems: 'center'
    },
    loginbtnText: {
        color: themeColor.AyushButtonText,
        alignItems: 'center',
        fontSize: 18,
        fontWeight: '500'
    },
    spinnerTextStyle: {
        color: '#FFF'
    },
})


class RegisterScreen extends React.Component{
    constructor(){
        super();
        this.state = {
            user_id:"",
            old_password: "",
            new_password: "",
            c_password: "",
            spinner: false
        }
        AsyncStorage.getItem("userToken")
        .then(value => {
            let data = JSON.parse(value);
            this.setState({
                 user_id: data.user_id
            })
        })
        .done();
    }

    changepassword = () =>{
        if(this.state.new_password.length < 3){
            this.refs.toast.show('Please enter minimum 4 characters in new password.');
            return;
        }
        if(this.state.new_password != this.state.c_password){
            this.refs.toast.show('Password Missmatch.');
            return;
        }
        this.setState({
            spinner: !this.state.spinner
        });
        if(this.state.new_password == this.state.c_password){
            fetch(URL+'/Update_Password', {
                method: 'POST',
                headers: {
                  Accept: 'application/json',
                  'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    user_id : this.state.user_id,
                    old_password: this.state.old_password,
                    new_password: this.state.new_password
                }),
              })
                .then(response => response.json())
                .then(responseJson => {
    
                    if(responseJson.error){
                        this.refs.toast.show(responseJson.message);
                      }
                        this.refs.toast.show(responseJson.message);
                        this.setState({
                            spinner: !this.state.spinner,
                            new_password: '',
                            c_password: '',
                            old_password: ''
                        });
                })
                .catch(error => {
                  //alert(JSON.stringify(error));
                  this.setState({
                        spinner: !this.state.spinner
                    });
                });
        }
        else{
                this.refs.toast.show("Password Mismatch");
            this.setState = ({
                c_password: "",
                spinner: !this.state.spinner
            })
        }
    }

    render(){
        return (
            <KeyboardAvoidingView behavior="padding" style={styles.container} enabled>
            <Spinner
                visible={this.state.spinner}
                cancelable={true}
                animation={"fade"}
                textContent={'Loading...'}
                textStyle={styles.spinnerTextStyle}
            />
            <View style={styles.container}>
                    <View>
                    <TextInput
                            style={styles.inputData}
                            placeholder="Old Password"
                            placeholderTextColor="#ccc"
                            underlineColorAndroid="transparent"
                            value={this.state.old_password}
                            secureTextEntry={true}
                            onChangeText={text => this.setState({ old_password: text })}
                        />
                        <View style={styles.inputIcon}>
                            <Icon name="vpn-key" size={25} color='#058f3a'/>
                        </View>
                    </View>
                <View>
                    <TextInput
                        style={styles.inputData}
                        placeholder="New Password"
                        placeholderTextColor="#ccc"
                        underlineColorAndroid="transparent"
                        value={this.state.new_password}
                        secureTextEntry={true}
                        onChangeText={text => this.setState({ new_password: text })}
                    />
                    <View style={styles.inputIcon}>
                        <Icon name="vpn-key" size={25} color='#058f3a'/>
                    </View>
                </View>
                <View>
                    <TextInput
                        style={styles.inputData}
                        placeholder="Confirm Password"
                        placeholderTextColor="#ccc"
                        underlineColorAndroid="transparent"
                        value={this.state.c_password}
                        secureTextEntry={true}
                        onChangeText={text => this.setState({ c_password: text })}
                    />
                    <View style={styles.inputIcon}>
                        <Icon name="vpn-key" size={25} color='#058f3a'/>
                    </View>
                </View>
                <TouchableOpacity onPress={this.changepassword.bind(this)}>
                    <View style={styles.loginbtn}>
                            <Text style={styles.loginbtnText}>Update</Text>
                    </View>
                </TouchableOpacity>
                <Toast ref="toast"
                positionValue = {10}
                />
            </View>
            </KeyboardAvoidingView>
        )
    }
}

export default RegisterScreen