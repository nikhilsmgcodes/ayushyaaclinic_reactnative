import React from 'react';
import {WebView} from 'react-native'

class DoctorScreen extends React.Component{
    render(){
        return (
            <WebView 
            originWhitelist={['*']}
            source={{uri:"http://ayushyaaclinic.com/mobile/team-member-detail.html"}} 
            style={{flex: 1}} />
        )
    }
}

export default DoctorScreen