import React from 'react';
import {WebView} from 'react-native'

class ContactScreen extends React.Component{
    render(){
        return (
            <WebView 
            originWhitelist={['*']}
            source={{uri:"http://ayushyaaclinic.com/mobile/contact-us.html"}} 
            style={{flex: 1}} />
        )
    }
}

export default ContactScreen