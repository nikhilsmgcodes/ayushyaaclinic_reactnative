import React from 'react';
import { View, Text, StyleSheet, ScrollView, AsyncStorage, TouchableOpacity } from 'react-native';
import getWH from '../constants/Layout';
import URL from '../constants/Url';
import { Icon } from 'react-native-elements';
import Spinner from 'react-native-loading-spinner-overlay';
import Toast from 'react-native-whc-toast';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: '#eeeeee',
    },
    textStyle:{
            fontSize: 18,
            textAlign: 'center'
    },
    tokenContainer: {
        backgroundColor: '#cc0001',
        width: getWH.window.width - 100,
        height: 80,
        borderWidth: 2,
        borderColor: '#fff',
        textAlign: 'center',
        alignItems: 'center',
        borderRadius: 8,
        marginVertical: 5
    },
    upcominToken: {
        width: getWH.window.width,
        height: 50,
        flexDirection: 'row',
        justifyContent: 'space-evenly',
        alignItems: 'center'
    },
    tokenNumber: {
        color: '#fff',
        fontSize: 54, 
        fontWeight: '900'
    },
    textUp: {
        fontSize: 20,
        color: '#fff',
        textAlign: 'center'
    },
    upOuter: {
        backgroundColor: '#006600',
        height: 30,
        width: 100,
        borderRadius: 8
    },
    displayContainer: {
        width : getWH.window.width - 30,
        height: 50,
        justifyContent: 'space-between',
        flexDirection: 'row',
        marginVertical: 10,
        backgroundColor: '#e0e0e0',
        borderLeftWidth: 6,
        borderColor: '#006600',
        padding: 8
    },
    spinnerTextStyle: {
        color: '#FFF'
    },
    errorMessage: {
        fontSize: 18,
         fontWeight: '500', 
         textAlign: 'center'
    }
})

class LinksScreen extends React.Component{
      constructor(props){
        super(props)
        this.state = {
            items: [],
            spinner: false,
            first: "----",
            second: "----",
            third: "----",
        }
    }

    static navigationOptions = ({ navigation }) =>{
      return {
        title: 'Ayushyaa Clinic',
        headerStyle: {
          backgroundColor: '#006600',
        },
        headerRight: (
          <TouchableOpacity onPress={navigation.getParam('refresh')}>
                  <View style={{paddingRight: 10}}>
                      <Icon name="refresh" size={28} color='#fff'/>
                  </View>
          </TouchableOpacity>
        ),
        headerTintColor: '#ffffff',
        headerTitleStyle: {
          fontWeight: 'bold',
          flex: 1,
          textAlign: 'center',
          color: "#fff",
          textShadowColor: 'rgba(0, 0, 0, 0.75)',
          textShadowOffset: {width: -1, height: 1},
          textShadowRadius: 10,
          fontSize: 24,
        },
      }
    }

    componentDidMount() {
        this.didFocusListener = this.props.navigation.addListener(
          'didFocus',
          () => { AsyncStorage.getItem("userToken")
          .then(value => {
              this.setState({
                   user_id: JSON.parse(value).user_id
              })
              this.getAllToken();
              this.getToken();
          })
          .done(); },
        );
        this.props.navigation.setParams({ refresh: this._refresh });
      }
    
      componentWillUnmount() {
        this.didFocusListener.remove();
      }

    getToken = () =>{
        this.setState({
            spinner: !this.state.spinner
        });
        fetch(URL+'/getTokenDetailsBydate', {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
              user_id: this.state.user_id
          }),
        })  
          .then(response => response.json())
          .then(responseJson => {
                if(!responseJson.error){
                    this.setState({
                        items: responseJson.data,
                        spinner: !this.state.spinner
                    })
                }
                else{
                    this.setState({
                        spinner: !this.state.spinner,
                    })
                }
        })
        .catch(error => {
            this.setState({
                spinner: !this.state.spinner,
            });
        });
    }

    getAllToken = () =>{
        fetch(URL+'/GetAllToken', {
          method: 'GET',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
          }
        })
          .then(response => response.json())
          .then(responseJson => {
            if (!responseJson.error) {
                if(responseJson.current.length > 0){
                    this.setState({
                        first: responseJson.current[0].token_number
                    })
                }
                else{
                    this.setState({
                        first: '----'
                    })
                }
                if(responseJson.pending.length > 0){
                    this.setState({
                        second: responseJson.pending[0].token_number
                    })
                }
                else{
                    this.setState({
                        second: '----'
                    })
                }
                if(responseJson.pending.length > 1){
                    this.setState({
                        third: responseJson.pending[1].token_number
                    })
                }
                else{
                    this.setState({
                        third: '----'
                    })
                }
            } else {
                this.setState({
                    first: "----",
                    second: "----",
                    third: "----",
                })
            }
          })
          .catch(error => {
            
          });
      }

      _refresh = () =>{
        this.getAllToken();
        this.getToken();
    }

    bookedTokens(){
        let items = this.state.items;
        if(items.length <= 0){
             return (
                     <View style={[styles.container, {flex: 1, justifyContent: 'center', alignItems: 'center'}]}>
                         <Text style={styles.errorMessage}>No Tokens Taken</Text>
                     </View>
             )
        }
        else{
             return (
                 <View>
                     {items.map((item, index) => (
                         <View style={styles.displayContainer} key={index}>
                                 <View>
                                         <Text>Token ID: #{item.token_number}</Text>
                                         <Text>{item.name}</Text>
                                 </View>
                                 <View>
                                     <Text>{item.start_time}</Text>
                                     <Text>{item.status}</Text>
                                 </View>
                         </View>
                     ))}
             </View>
             )
        }
}

    render(){
        var { items } = this.state;
                return (
                        <View style={styles.container}>
                        <Spinner
                            visible={this.state.spinner}
                            cancelable={true}
                            animation={"fade"}
                            textContent={'Loading...'}
                            textStyle={styles.spinnerTextStyle}
                        />
                            <View>
                                <Text style={styles.textStyle}>CURRENT TOKEN</Text>
                                <View style={styles.tokenContainer}>
                                    <Text style={styles.tokenNumber}>{this.state.first}</Text>
                                </View>
                            </View>
                            <View style={{backgroundColor: '#e7e7e7'}}>
                                    <Text style={styles.textStyle}>UPCOMING TOKENS</Text>
                                <View style={styles.upcominToken}>
                                        <View style={styles.upOuter}>
                                            <Text style={styles.textUp}>{this.state.second}</Text>
                                        </View>
                                        <View style={styles.upOuter}>
                                            <Text style={styles.textUp}>{this.state.third}</Text>
                                        </View>
                                </View>
                            </View>
                            <ScrollView>
                                    {this.bookedTokens()}
                            </ScrollView>
                            <Toast ref="toast"
                positionValue = {10}
                />
                        </View>
                )
        }
}

// class Check extends React.Component { 

//     constructor(props){
//         super(props)
//         this.state = {
//             items: [],
//             len: Boolean
//         }
//     }

//     componentWillReceiveProps(nextProps) {
//         this.setState({
//           items: nextProps,
//           len: (nextProps.length > 0) ? true : false 
//         });
//         console.log(this.state.items.length);
//     }

//     render(){
//        if(!this.len){
//             return (
//                     <View style={[styles.container, {flex: 1, justifyContent: 'center', alignItems: 'center'}]}>
//                         <Text style={styles.errorMessage}>No Tokens Taken</Text>
//                     </View>
//             )
//        }
//        else{
//             return (
//                 <View>
//                     {items.map((item, index) => (
//                         <View style={styles.displayContainer} key={index}>
//                                 <View>
//                                         <Text>Token ID: #{item.token_number}</Text>
//                                         <Text>{item.name}</Text>
//                                 </View>
//                                 <View>
//                                     <Text>{item.start_time}</Text>
//                                     <Text>{item.status}</Text>
//                                 </View>
//                         </View>
//                     ))}
//             </View>
//             )
//        }
//     }
// }

export default LinksScreen