import React from 'react';
import {StyleSheet, View, TextInput, Text, TouchableOpacity, ToastAndroid, KeyboardAvoidingView, AsyncStorage} from 'react-native';
import getWH from '../constants/Layout';
import themeColor from '../constants/Colors';
import URL from '../constants/Url';
import { Icon } from 'react-native-elements';
import DateTimePicker from 'react-native-modal-datetime-picker';
import Spinner from 'react-native-loading-spinner-overlay';
import Toast from 'react-native-whc-toast';

const styles = StyleSheet.create({
    container: {
        backgroundColor: themeColor.AyushBackground,
        flex: 1,
        paddingVertical: 10,
        alignItems: 'center'
    },
    clinicName: {
        fontSize: 28,
        fontWeight: '500',
        color: '#ffff00',
        marginVertical: 5
    },
    inputData: {
        width: getWH.window.width - 60,
        borderColor: themeColor.AyushInputBorderColor,
        borderWidth: 1,
        height: 40,
        color: themeColor.AyushInputColor,
        paddingHorizontal: 10,
        borderRadius: 5,
        marginVertical: 5 
    },
    inputIcon: {
        position: 'absolute',
        right: 5,
        top: 12,
    },
    loginbtn: {
        width: getWH.window.width - 60,
        borderColor: themeColor.AyushInputBorderColor,
        backgroundColor: themeColor.AyushButton,
        borderWidth: 1,
        height: 40,
        paddingHorizontal: 10,
        borderRadius: 5,
        marginVertical: 5,
        justifyContent: 'center',
        alignItems: 'center'
    },
    loginbtnText: {
        color: themeColor.AyushButtonText,
        alignItems: 'center',
        fontSize: 18,
        fontWeight: '500'
    },
    spinnerTextStyle: {
        color: '#FFF'
    },
})


class ProfileScreen extends React.Component{
    constructor(){
        super();
        this.state = {
            user_id: "",
            name: "",
            phone: "",
            email: "",
            dob: "",
            spinner: false
        }
        AsyncStorage.getItem("userToken")
        .then(value => {
            let data = JSON.parse(value);
            this.setState({
                 user_id: data.user_id,
                 name: data.name,
                 email: data.email_id,
                 dob: data.dob,
                 phone: data.phone
            })
        })
        .done();
    }

    _dateFormat = (_date) => {
        let x = new Date(_date);
        let dd = (x.getDate < 10)? '0'+x.getDate() : x.getDate();
        let m = x.getMonth()+1;
        let mm = m<10? '0'+m : m;
        let yyyy = x.getFullYear();
        return yyyy+'-'+mm+'-'+dd;
    }

    _showDateTimePicker = () => this.setState({ isDateTimePickerVisible: true });
    
      _hideDateTimePicker = () => this.setState({ isDateTimePickerVisible: false });
    
      _handleDatePicked = (date) => {
        //console.log('A date has been picked: ', date.toDateString());
         this.setState({
             dob: this._dateFormat(date.toISOString())
         })
        this._hideDateTimePicker();
      };

    Update = () =>{
            this.setState({
                spinner: !this.state.spinner
            });
            fetch(URL+'/Update_Profile', {
                method: 'POST',
                headers: {
                  Accept: 'application/json',
                  'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    name: this.state.name,
                    phone: this.state.phone,
                    email_id: this.state.email,
                    user_id: this.state.user_id,
                    dob: this.state.dob
                }),
              })
                .then(response => response.json())
                .then(responseJson => {
                  if(!responseJson.error){
                    AsyncStorage.setItem('userToken', JSON.stringify(responseJson.data));
                  }
                  this.refs.toast.show(responseJson.message);
                    this.setState({
                        spinner: !this.state.spinner
                    });
                })
                .catch(error => {
                  //alert(JSON.stringify(error));
                 setTimeout(()=>{
                    this.setState({
                        spinner: !this.state.spinner
                      });
                 }, 100)
                });
    }

    render(){
        return (
            <KeyboardAvoidingView behavior="padding" style={styles.container} enabled>
            <Spinner
                visible={this.state.spinner}
                cancelable={true}
                animation={"fade"}
                textContent={'Loading...'}
                textStyle={styles.spinnerTextStyle}
            />
            <View style={styles.container}>
                <View>
                    <TextInput
                        style={styles.inputData}
                        placeholder="Name"
                        placeholderTextColor="#ccc"
                        underlineColorAndroid="transparent"
                        value={this.state.name}
                        onChangeText={text => this.setState({ name: text })}
                    />
                    <View style={styles.inputIcon}>
                        <Icon name="person-outline" size={25} color='#058f3a'/>
                    </View>
                </View>
                <View>
                    <TextInput
                        style={styles.inputData}
                        placeholder="Phone"
                        keyboardType="numeric"
                        editable={false}
                        placeholderTextColor="#ccc"
                        underlineColorAndroid="transparent"
                        value={this.state.phone}
                        onChangeText={text => this.setState({ phone: text })}
                    />
                    <View style={styles.inputIcon}>
                        <Icon name="phone" size={25} color='#058f3a'/>
                    </View>
                </View>
                <View>
                    <TextInput
                        style={styles.inputData}
                        placeholder="Email"
                        placeholderTextColor="#ccc"
                        underlineColorAndroid="transparent"
                        value={this.state.email}
                        keyboardType="email-address"
                        onChangeText={text => this.setState({ email: text })}
                    />
                    <View style={styles.inputIcon}>
                        <Icon name="email" size={25} color='#058f3a'/>
                    </View>
                </View>
                <View>
                <TouchableOpacity onPress={this._showDateTimePicker}
                activeOpacity={0.97}
                style={[{ zIndex: 999, elevation: 999, height:50, width: getWH.window.width - 60}]}
                >
                    <TextInput
                        pointerEvents="none"
                        style={styles.inputData}
                        placeholder="Date of Birth"
                        editable={false}
                        value={this.state.dob}
                        underlineColorAndroid="transparent"
                        onChangeText={text => this.setState({ dob: text })}
                    />
                </TouchableOpacity>
                <DateTimePicker
                    isVisible={this.state.isDateTimePickerVisible}
                    onConfirm={this._handleDatePicked}
                    onCancel={this._hideDateTimePicker}
                    datePickerModeAndroid={"spinner"}
                    />
                <View style={styles.inputIcon}>
                    <Icon name="date-range" size={25} color='#058f3a'/>
                </View>
                </View>
                <TouchableOpacity onPress={this.Update.bind(this)}>
                    <View style={styles.loginbtn}>
                            <Text style={styles.loginbtnText}>Update</Text>
                    </View>
                </TouchableOpacity>
                <Toast ref="toast"
                positionValue = {10}
                />
            </View>
            </KeyboardAvoidingView>
        )
    }
}

export default ProfileScreen