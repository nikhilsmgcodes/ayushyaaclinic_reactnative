import React from 'react'
import { View, Text, TextInput, StyleSheet, 
    TouchableOpacity,ToastAndroid, Picker, 
    AsyncStorage, KeyboardAvoidingView, Alert } from 'react-native'
import getWH from '../constants/Layout';
import { Icon } from 'react-native-elements';
import URL from '../constants/Url';
import themeColor from '../constants/Colors';
import DateTimePicker from 'react-native-modal-datetime-picker';
import Spinner from 'react-native-loading-spinner-overlay';
import { Dropdown } from 'react-native-material-dropdown';
import Toast from 'react-native-whc-toast';

const styles = StyleSheet.create({
    outerContainer: {
        backgroundColor: '#fff',
        flex: 1,
    },
    container: {
        flex: 1,
        width: getWH.window.width,
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    innerContainer: {
        flex: 1,
        height: 80,
        backgroundColor: '#fff',
        borderWidth: 1,
        borderColor: '#006600',
        paddingVertical: 10
    },
    textStyle: {
        textAlign: 'center',
        fontSize: 16,
    },
    inputData: {
        width: getWH.window.width - 60,
        borderColor: themeColor.AyushInputBorderColor,
        borderWidth: 1,
        height: 40,
        color: themeColor.AyushInputColor,
        paddingHorizontal: 10,
        borderRadius: 5,
    },
    inputContainer: {
        flex: 5, 
        justifyContent: 'center', 
        flexDirection: 'row'
    },
    loginbtn: {
        width: getWH.window.width - 60,
        borderColor: themeColor.AyushInputBorderColor,
        backgroundColor: '#006600',
        borderWidth: 1,
        height: 40,
        paddingHorizontal: 10,
        borderRadius: 5,
        marginVertical: 5,
        justifyContent: 'center',
        alignItems: 'center'
    },
    loginbtnText: {
        color: '#fff',
        alignItems: 'center',
        fontSize: 18,
        fontWeight: '500'
    },
    spinnerTextStyle: {
        color: '#FFF'
    },
})

class TokenScreen extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            tabFlag: 1,
            items: [],
            numberofpatient: 1,
            spinner: false,
            name: "",
            phone: "",
            email: "",
            dob: "",
            isDateTimePickerVisible:false
        }
        AsyncStorage.getItem("userToken")
        .then(value => {
            this.setState({
            user_id: JSON.parse(value).user_id
            });
        })
        .done();
    }

    self = () => {
        this.setState({tabFlag: 1})
    }

    other = () => {
        this.setState({tabFlag:0})
    }

    ConfirmToken = () =>{
        Alert.alert(
            'Are you Sure?',
            'You Want to Proceed with selected token!',
            [
              {
                text: 'Cancel',
                onPress: () => console.log('Cancel Pressed'),
                style: 'cancel',
              },
              {text: 'OK', onPress: () => {this.selftoken()}},
            ],
            {cancelable: false},
          );
    }

    selftoken = () =>{
            this.setState({
                spinner: !this.state.spinner
            });
            fetch(URL + "/Token_Generate", {
                method: "POST",
                headers: {
                  Accept: "application/json",
                  "Content-Type": "application/json"
                },
                body: JSON.stringify({
                  NoOfPatient: this.state.numberofpatient,
                  user_id: this.state.user_id,
                  TokenFor: "self"
                })
              })
                .then(response => response.json())
                .then(responseJson => {
                    this.setState({
                        spinner: !this.state.spinner,
                        numberofpatient: 1
                    });
                    this.refs.toast.show(responseJson.message);
                  
                })
                .catch(error => {
                    this.setState({
                         spinner: !this.state.spinner
                    });
                });
    }

    othertoken = () => {
        if(this.state.name != "" || this.state.phone != "" || this.state.email != "" || this.state.dob != ""){
            this.setState({
                spinner: !this.state.spinner
            });
            fetch(URL + "/Token_Generate", {
                method: "POST",
                headers: {
                  Accept: "application/json",
                  "Content-Type": "application/json"
                },
                body: JSON.stringify({
                    phone: this.state.phone,
                    patient_name: this.state.name,
                    dob: this.state.dob,
                    email: this.state.email,
                    NoOfPatient: this.state.numberofpatient,
                    user_id: this.state.user_id,
                    TokenFor: "Other"
                })
              })
                .then(response => response.json())
                .then(responseJson => {
                    this.setState({
                        spinner: !this.state.spinner,
                        numberofpatient: 1
                      });
                  if (responseJson.error) {
                    this.refs.toast.show(responseJson.message);
                  } 
                  else {
                    this.refs.toast.show(responseJson.message);
                    this.setState({
                        name: "",
                        email: "",
                        phone: "",
                        dob: ""
                    })
                  }
                })
                .catch(error => {
                    this.setState({
                    spinner: !this.state.spinner
                    });
                });
        }
        else{
            this.refs.toast.show("Fill All Details");
        }
    }

    _dateFormat = (_date) => {
        let x = new Date(_date);
        let dd = (x.getDate < 10)? '0'+x.getDate() : x.getDate();
        let m = x.getMonth()+1;
        let mm = m<10? '0'+m : m;
        let yyyy = x.getFullYear();
        return yyyy+'-'+mm+'-'+dd;
    }

    _showDateTimePicker = () => this.setState({ isDateTimePickerVisible: true });
    
      _hideDateTimePicker = () => this.setState({ isDateTimePickerVisible: false });
    
      _handleDatePicked = (date) => {
        //console.log('A date has been picked: ', date.toDateString());
         this.setState({
             dob: this._dateFormat(date.toISOString())
         })
        this._hideDateTimePicker();
      };


    render(){
        let data = [{
            value: '1',
          }, {
            value: '2',
          }, {
            value: '3',
          }, {
            value: '4',
          }, {
            value: '5',
          }];
      
        return (
            <KeyboardAvoidingView behavior="padding" style={styles.container} enabled>
                <Spinner
                    visible={this.state.spinner}
                    cancelable={true}
                    animation={"fade"}
                    textContent={'Loading...'}
                    textStyle={styles.spinnerTextStyle}
                />
            <View style={styles.outerContainer}>
                <View style={styles.container}>
                    <View style={styles.innerContainer}>
                        <TouchableOpacity 
                            onPress = {this.self.bind(this)}
                            underlayColor = '#006600'
                            >
                            <View>
                                <Icon name="person" size={34} 
                                color={(this.state.tabFlag == 1) ? "#eb8b28" : "#ccc"}
                                />
                                <Text style={[{color: (this.state.tabFlag == 1)? "#eb8b28" : "#ccc"}, styles.textStyle]}>SELF</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                    <View style={styles.innerContainer}>
                        <TouchableOpacity 
                            onPress = {this.other.bind(this)}
                            underlayColor = '#006600'
                            >
                            <View>
                                <Icon name="person-add" size={34} 
                                color={(this.state.tabFlag == 1 )? "#ccc" : "#eb8b28"}
                                />
                                <Text style={[{color: (this.state.tabFlag == 1)? "#ccc" : "#eb8b28"}, styles.textStyle]}>OTHERS</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                </View>
                <View style={styles.inputContainer}>
                    {(this.state.tabFlag == 1)? 
                        (
                            <View>
                                {/* <Text style={{color: '#ccc'}}>Select Number of Token</Text> */}
                                {/* <View style={[styles.inputData, {marginBottom: 10}]}>
                                    <Picker
                                        selectedValue={this.state.numberofpatient}
                                        style={{height: 40}}
                                        prompt={"Select Number of Token"}
                                        onValueChange={(itemValue, itemIndex) =>
                                            this.setState({numberofpatient: itemValue})
                                        }>
                                        <Picker.Item label="1" value="1" />
                                        <Picker.Item label="2" value="2" />
                                        <Picker.Item label="3" value="3" />
                                        <Picker.Item label="4" value="4" />
                                        <Picker.Item label="5" value="5" />
                                    </Picker>
                                </View> */}
                                <View>
                                <Dropdown
        label='Select Number of Token'
        data={data}
        value= {this.state.numberofpatient}
        onChangeText={(itemValue, itemIndex) =>
            this.setState({numberofpatient: itemValue})
        }
      />
                                    </View>
                                <TouchableOpacity onPress={this.ConfirmToken.bind(this)}>
                                    <View style={styles.loginbtn}>
                                            <Text style={styles.loginbtnText}>Get Token</Text>
                                    </View>
                                </TouchableOpacity>
                            </View>

                        ): 
                        (
                            <View>
                                    {/* <Text style={{color: '#ccc'}}>Select Number of Token</Text> */}
                                    {/* <View style={[styles.inputData, {marginBottom: 10}]}>
                                        <Picker
                                            selectedValue={this.state.numberofpatient}
                                            prompt={"Select Number of Token"}
                                            style={{height: 40}}
                                            onValueChange={(itemValue, itemIndex) =>
                                                this.setState({numberofpatient: itemValue})
                                            }>
                                            <Picker.Item label="1" value="1" />
                                            <Picker.Item label="2" value="2" />
                                            <Picker.Item label="3" value="3" />
                                            <Picker.Item label="4" value="4" />
                                            <Picker.Item label="5" value="5" />
                                        </Picker>
                                    </View> */}
                                      <View>
                                <Dropdown
        label='Select Number of Token'
        data={data}
        value= {this.state.numberofpatient}
        onChangeText={(itemValue, itemIndex) =>
            this.setState({numberofpatient: itemValue})
        }
      />
                                    </View>
                                    <View>
                                        <TextInput
                                           style={[styles.inputData, {color: '#000'}]}
                                            placeholder="Name"
                                            placeholderTextColor="#ccc"
                                            underlineColorAndroid="transparent"
                                            value={this.state.name}
                                            onChangeText={text => this.setState({ name: text })}
                                        />
                                        <View style={styles.inputIcon}>
                                            <Icon name="person-outline" size={25} color='#fff'/>
                                        </View>
                                    </View>
                                    <View>
                                        <TextInput
                                            style={[styles.inputData, {color: '#000'}]}
                                            placeholder="Phone"
                                            keyboardType="numeric"
                                            placeholderTextColor="#ccc"
                                            underlineColorAndroid="transparent"
                                            value={this.state.phone}
                                            maxLength={10}
                                            onChangeText={text => this.setState({ phone: text })}
                                        />
                                        <View style={styles.inputIcon}>
                                            <Icon name="phone" size={25} color='#fff'/>
                                        </View>
                                    </View>
                                    <View>
                                        <TextInput
                                            style={[styles.inputData, {color: '#000'}]}
                                            placeholder="Email"
                                            placeholderTextColor="#ccc"
                                            underlineColorAndroid="transparent"
                                            value={this.state.email}
                                            keyboardType="email-address"
                                            onChangeText={text => this.setState({ email: text })}
                                        />
                                        <View style={styles.inputIcon}>
                                            <Icon name="email" size={25} color='#fff'/>
                                        </View>
                                    </View>
                                    <View>
                                    <TouchableOpacity onPress={this._showDateTimePicker}
                                    activeOpacity={0.97}
                                    style={[{ zIndex: 999, elevation: 999, height:40, width: getWH.window.width - 60}]}
                                    >
                                        <TextInput
                                            pointerEvents="none"
                                            style={[styles.inputData, {color: '#000'}]}
                                            placeholder="Date of Birth"
                                            placeholderTextColor="#ccc"
                                            editable={false}
                                            value={this.state.dob}
                                            underlineColorAndroid="transparent"
                                            onChangeText={text => this.setState({ dob: text })}
                                        />
                                    </TouchableOpacity>
                                    <DateTimePicker
                                        isVisible={this.state.isDateTimePickerVisible}
                                        onConfirm={this._handleDatePicked}
                                        onCancel={this._hideDateTimePicker}
                                        datePickerModeAndroid={"spinner"}
                                        />
                                    <View style={styles.inputIcon}>
                                        <Icon name="date-range" size={25} color='#fff'/>
                                    </View>
                                    </View>
                                    <TouchableOpacity onPress={this.othertoken.bind(this)}>
                                        <View style={styles.loginbtn}>
                                                <Text style={styles.loginbtnText}>Get Token</Text>
                                        </View>
                                    </TouchableOpacity>
                            </View>
                            
                        )
                    }
                </View>
                <Toast ref="toast"
                positionValue = {10}
                />
            </View>
            </KeyboardAvoidingView>
        )
    }
}


export default TokenScreen