import React from 'react';
import {StyleSheet, View, TextInput, Text, TouchableOpacity, ToastAndroid, KeyboardAvoidingView, AsyncStorage, Image} from 'react-native';
import getWH from '../constants/Layout';
import themeColor from '../constants/Colors';
import URL from '../constants/Url';
import { Icon } from 'react-native-elements';
import Spinner from 'react-native-loading-spinner-overlay';
import Toast from 'react-native-whc-toast';

const styles = StyleSheet.create({
    container: {
        backgroundColor: themeColor.AyushBackground,
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    headlogo: {
        marginVertical: 30,
        width: getWH.window.width - 50,
    },
    inputData: {
        width: getWH.window.width - 60,
        borderColor: themeColor.AyushInputBorderColor,
        borderWidth: 1,
        height: 40,
        color: themeColor.AyushInputColor,
        paddingHorizontal: 10,
        borderRadius: 5,
        marginVertical: 5 
    },
    inputIcon: {
        position: 'absolute',
        right: 5,
        top: 12,
    },
    loginbtn: {
        width: getWH.window.width - 60,
        borderColor: themeColor.AyushInputBorderColor,
        backgroundColor: themeColor.AyushButton,
        borderWidth: 1,
        height: 40,
        paddingHorizontal: 10,
        borderRadius: 5,
        marginVertical: 5,
        justifyContent: 'center',
        alignItems: 'center'
    },
    loginbtnText: {
        color: themeColor.AyushButtonText,
        alignItems: 'center',
        fontSize: 18,
        fontWeight: '500'
    },
    spinnerTextStyle: {
        color: '#FFF'
    },
})


class LoginScreen extends React.Component{
    constructor(){
        super();
        this.state = {
            phone: "",
            password: "",
            spinner: false
        }
    }

    login = async () => {
            if(this.state.phone == "" || this.state.password == ""){
                this.refs.toast.show("Invalid Username or Password",);
            }
            else{
                this.setState({
                    spinner: !this.state.spinner
                  });
                  fetch(URL+'/User_login', {
                      method: 'POST',
                      headers: {
                          Accept: 'application/json',
                          'Content-Type': 'application/json',
                      },
                      body: JSON.stringify({
                          phone: this.state.phone,
                          password: this.state.password
                      }),
                      })
                      .then(response => response.json())
                      .then(responseJson => {
                          this.setState({
                              spinner: !this.state.spinner
                          });
                          if(responseJson.error){
                              this.refs.toast.show(responseJson.message, 
                              ToastAndroid.LONG,
                              ToastAndroid.TOP,
                              25,
                              50,);
                          }else{
                              AsyncStorage.setItem('userToken', JSON.stringify(responseJson.data));
                              this.props.navigation.navigate('Home');
                          }
                      })
                      .catch(error => {
                          //alert(JSON.stringify(error));
                          this.setState({
                              spinner: !this.state.spinner
                          });
                      });
            }
    }

    //social login
    // socialLogin(name, email, unique_id){
    //     this.setState({
    //         spinner: !this.state.spinner
    //     });
    //     fetch(URL+'/User_login', {
    //     method: 'POST',
    //     headers: {
    //       Accept: 'application/json',
    //       'Content-Type': 'application/json',
    //     },
    //     body: JSON.stringify({
    //       name: name,
    //       email_id: email,
    //       unique_id : unique_id,
    //       login_type : 'social'
    //     }),
    //   })
    //     .then(response => response.json())
    //     .then(responseJson => {
    //       if(responseJson.error){
    //         this.setState({
    //             spinner: !this.state.spinner
    //         });
    //         this.refs.toast.show(responseJson.message, 
    //             ToastAndroid.LONG,
    //             ToastAndroid.TOP,
    //             25,
    //             50,);
    //       }else{
    //         AsyncStorage.setItem('userToken', JSON.stringify(responseJson.data))
    //         this.props.navigation.navigate('Home');
    //       }
    //     })
    //     .catch(error => {
    //      // alert(JSON.stringify(error));
    //      this.setState({
    //             spinner: !this.state.spinner
    //         });
    //     });
    //   }

    // async FacebooklogIn() {
    //     try {
    //         const {
    //           type,
    //           token,
    //           expires,
    //           permissions,
    //           declinedPermissions,
    //         } = await Expo.Facebook.logInWithReadPermissionsAsync('2249790811718711', {
    //           permissions: ['public_profile','email'],
    //         });
    //         if (type === 'success') {
    //           // Get the user's name using Facebook's Graph API
    //           const response = await fetch(`https://graph.facebook.com/me?fields=id,name,email&access_token=${token}`);
    //           var det = await response.json();
    //           this.setState({
    //             spinner: !this.state.spinner
    //           });
    //           this.socialLogin(det.name, det.email, det.id);
    //         } else {
    //           // type === 'cancel'
    //         }
    //       } catch ({ message }) {
    //         alert(`Facebook Login Error: ${message}`);
    //       }
    //   }



    goto_forgot = () =>{
        this.setState({
            phone: "",
            password: ""
        })
        this.props.navigation.navigate('Forgot');
    }

    logo(){
        const IPHONE7PLUS_WIDTH = 450;
        if (getWH.window.width <= IPHONE7PLUS_WIDTH) {
            return(
            <View style={styles.headlogo}>
                <Image source={require('../assets/images/ayushyaa.png')} style={{width: getWH.window.width - 60, height: 120}} />
            </View>
            )
        }
        else{
            return(
            <View style={styles.headlogo}>
                <Image source={require('../assets/images/ayushyaa.png')} style={{width: getWH.window.width - 60, height: 260}} />
            </View>
            )
        }
    }

    render(){
        return (
            <KeyboardAvoidingView behavior="padding" style={styles.container} enabled>
            <Spinner
                visible={this.state.spinner}
                cancelable={true}
                animation={"fade"}
                textContent={'Loading...'}
                textStyle={styles.spinnerTextStyle}
            />
            <View style={styles.container}>
            {this.logo()}
                <View>
                    <TextInput
                        style={styles.inputData}
                        placeholder="Phone"
                        keyboardType="numeric"
                        placeholderTextColor="#ccc"
                        underlineColorAndroid="transparent"
                        value={this.state.phone}
                        maxLength={10}
                        onChangeText={text => this.setState({ phone: text })}
                    />
                    <View style={styles.inputIcon}>
                        <Icon name="phone" size={25} color='#058f3a'/>
                    </View>
                </View>
                <View>
                    <TextInput
                        style={styles.inputData}
                        placeholder="Password"
                        placeholderTextColor="#ccc"
                        underlineColorAndroid="transparent"
                        value={this.state.password}
                        secureTextEntry={true}
                        onChangeText={text => this.setState({ password: text })}
                    />
                    <View style={styles.inputIcon}>
                        <Icon name="lock-outline" size={25} color='#058f3a'/>
                    </View>
                </View>
                <TouchableOpacity onPress={this.login.bind(this)}>
                    <View style={styles.loginbtn}>
                            <Text style={styles.loginbtnText}>Login</Text>
                    </View>
                </TouchableOpacity>
                <View style={{marginVertical: 10}}>
                    <TouchableOpacity onPress={this.goto_forgot.bind(this)}>
                        <Text style={{color: '#000'}}>Forgot Password ?</Text>
                    </TouchableOpacity>
                </View>
                {/* <View>
                    <TouchableOpacity onPress={this.FacebooklogIn.bind(this)}>
                        <Image source={require('../assets/images/facebook.png')} style={{width:50,height:50}} />
                    </TouchableOpacity>
                </View> */}
                                <Toast ref="toast"
                positionValue = {10}
                />
            </View>
            </KeyboardAvoidingView>
        )
    }
}

export default LoginScreen