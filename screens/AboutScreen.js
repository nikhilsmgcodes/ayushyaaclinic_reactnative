import React from 'react';
import {WebView} from 'react-native'

class AboutScreen extends React.Component{
    render(){
        return (
            <WebView 
                originWhitelist={['*']}
                source={{uri: "http://ayushyaaclinic.com/mobile/about-us.html"}} 
                style={{flex: 1}} 
            />
        )
    }
}

export default AboutScreen