import React from 'react';
import {WebView} from 'react-native'

class ServiceScreen extends React.Component{
    render(){
        return (
            <WebView 
            originWhitelist={['*']}
            source={{uri:"http://ayushyaaclinic.com/mobile/services.html"}} 
            style={{flex: 1}} />
        )
    }
}

export default ServiceScreen