import React from 'react';
import {StyleSheet, View, TextInput, Text, TouchableOpacity, ToastAndroid, KeyboardAvoidingView, AsyncStorage, Image} from 'react-native';
import getWH from '../constants/Layout';
import themeColor from '../constants/Colors';
import URL from '../constants/Url';
import { Icon } from 'react-native-elements';
import DateTimePicker from 'react-native-modal-datetime-picker';
import { createAppContainer, createDrawerNavigator } from 'react-navigation';
import Spinner from 'react-native-loading-spinner-overlay';
import Toast from 'react-native-whc-toast';

const styles = StyleSheet.create({
    container: {
        backgroundColor: themeColor.AyushBackground,
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    headlogo: {
        marginVertical: 30,
        width: getWH.window.width - 50,
    },
    inputData: {
        width: getWH.window.width - 60,
        borderColor: themeColor.AyushInputBorderColor,
        borderWidth: 1,
        height: 40,
        color: themeColor.AyushInputColor,
        paddingHorizontal: 10,
        borderRadius: 5,
        marginVertical: 5 
    },
    inputIcon: {
        position: 'absolute',
        right: 5,
        top: 12,
    },
    loginbtn: {
        width: getWH.window.width - 60,
        borderColor: themeColor.AyushInputBorderColor,
        backgroundColor: themeColor.AyushButton,
        borderWidth: 1,
        height: 40,
        paddingHorizontal: 10,
        borderRadius: 5,
        marginVertical: 5,
        justifyContent: 'center',
        alignItems: 'center'
    },
    loginbtnText: {
        color: themeColor.AyushButtonText,
        alignItems: 'center',
        fontSize: 18,
        fontWeight: '500'
    },
    spinnerTextStyle: {
        color: '#FFF'
    },
})

class GetOTP extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            phone: "",
            spinner: false
        }
    }

    send_otp = () =>{
        if(this.state.phone.length != 10){
            this.refs.toast.show("Please Enter 10 digit Phone Number");
            return;
        }
        this.setState({
            spinner: !this.state.spinner
        });
        fetch(URL+'/ForgotPassword', {
            method: 'POST',
            headers: {
              Accept: 'application/json',
              'Content-Type': 'application/json',
            },
            body: JSON.stringify({
              type: '0',
              phone: this.state.phone
            }),
          })
            .then(response => response.json())
            .then(responseJson => {
              if(responseJson.error){
                this.refs.toast.show("Phone number already Registered.");
              }
              else{
                this.refs.toast.show(responseJson.message);
                this.props.navigation.navigate('VerifyOTP', {'phone' : this.state.phone});
              }
              this.setState({
                spinner: !this.state.spinner
              });
            })
            .catch(error => {
                this.setState({
                    spinner: !this.state.spinner
                });
            });
    }

    logo(){
        const IPHONE7PLUS_WIDTH = 450;
        if (getWH.window.width <= IPHONE7PLUS_WIDTH) {
            return(
            <View style={styles.headlogo}>
                <Image source={require('../assets/images/ayushyaa.png')} style={{width: getWH.window.width - 60, height: 120}} />
            </View>
            )
        }
        else{
            return(
            <View style={styles.headlogo}>
                <Image source={require('../assets/images/ayushyaa.png')} style={{width: getWH.window.width - 60, height: 260}} />
            </View>
            )
        }
    }

    render(){
        return (
            <KeyboardAvoidingView behavior="padding" style={styles.container} enabled>
            <Spinner
                visible={this.state.spinner}
                cancelable={true}
                animation={"fade"}
                textContent={'Loading...'}
                textStyle={styles.spinnerTextStyle}
            />
            <View style={styles.container}>
            {this.logo()}
                <View>
                    <TextInput
                        style={styles.inputData}
                        placeholder="Phone"
                        keyboardType="numeric"
                        placeholderTextColor="#ccc"
                        underlineColorAndroid="transparent"
                        value={this.state.phone}
                        autoFocus={true}
                        maxLength={10}
                        onChangeText={text => this.setState({ phone: text })}
                    />
                    <View style={styles.inputIcon}>
                        <Icon name="phone" size={25} color='#058f3a'/>
                    </View>
                </View>
                <TouchableOpacity onPress={this.send_otp.bind(this)}>
                    <View style={styles.loginbtn}>
                            <Text style={styles.loginbtnText}>Get OTP</Text>
                    </View>
                </TouchableOpacity>
                <Toast ref="toast"
                positionValue = {10}
                />
            </View>
            </KeyboardAvoidingView>
        )
    }
}

class VerifyOTP extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            phone: "",
            otp: "",
            spinner: false
        }
        const {state} = props.navigation;
        this.state.phone = state.params.phone;
    }

    verify_otp = () => {
        this.setState({
            spinner: !this.state.spinner
        });
        fetch(URL+'/VerifyOTPForgot', {
            method: 'POST',
            headers: {
              Accept: 'application/json',
              'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                phone: this.state.phone,
                otp: this.state.otp,
            }),
          })
            .then(response => response.json())
            .then(responseJson => {

              if(responseJson.error){
                this.refs.toast.show(responseJson.message, 
                );
              }
              else{
                    this.props.navigation.navigate('Register', {'phone': this.state.phone})
              }
              this.setState({
                spinner: !this.state.spinner
              });
            })
            .catch(error => {
              //alert(JSON.stringify(error));
              this.setState({
                spinner: !this.state.spinner
              });
            });
    }

    logo(){
        const IPHONE7PLUS_WIDTH = 450;
        if (getWH.window.width <= IPHONE7PLUS_WIDTH) {
            return(
            <View style={styles.headlogo}>
                <Image source={require('../assets/images/ayushyaa.png')} style={{width: getWH.window.width - 60, height: 120}} />
            </View>
            )
        }
        else{
            return(
            <View style={styles.headlogo}>
                <Image source={require('../assets/images/ayushyaa.png')} style={{width: getWH.window.width - 60, height: 260}} />
            </View>
            )
        }
    }

    render(){
        return (
            <KeyboardAvoidingView behavior="padding" style={styles.container} enabled>
            <Spinner
                visible={this.state.spinner}
                cancelable={true}
                animation={"fade"}
                textContent={'Loading...'}
                textStyle={styles.spinnerTextStyle}
            />
            <View style={styles.container}>
            {this.logo()}
                <View>
                    <TextInput
                        style={styles.inputData}
                        placeholder="Phone"
                        keyboardType="numeric"
                        placeholderTextColor="#ccc"
                        underlineColorAndroid="transparent"
                        value={this.state.phone}
                        maxLength={10}
                        editable = {false}
                        onChangeText={text => this.setState({ phone: text })}
                    />
                    <View style={styles.inputIcon}>
                        <Icon name="phone" size={25} color='#058f3a'/>
                    </View>
                </View>
                <View>
                    <TextInput
                        style={styles.inputData}
                        placeholder="Enter OTP"
                        keyboardType="numeric"
                        placeholderTextColor="#ccc"
                        underlineColorAndroid="transparent"
                        maxLength={4}
                        value={this.state.otp}
                        onChangeText={text => this.setState({ otp: text })}
                    />
                    
                    <View style={styles.inputIcon}>
                        <Icon name="phone" size={25} color='#058f3a'/>
                    </View>
                </View>
                <TouchableOpacity onPress={this.verify_otp.bind(this)}>
                    <View style={styles.loginbtn}>
                            <Text style={styles.loginbtnText}>Verify OTP</Text>
                    </View>
                </TouchableOpacity>
                <Toast ref="toast"
                positionValue = {10}
                />
            </View>
            </KeyboardAvoidingView>
        )
    }
}

class Register extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            name: "",
            phone: "",
            email: "",
            dob: "",
            password: "",
            c_password: "",
            spinner: false
        }
        const {state} = props.navigation;
        this.state.phone = state.params.phone;
    }

    _dateFormat = (_date) => {
        let x = new Date(_date);
        let dd = (x.getDate < 10)? '0'+x.getDate() : x.getDate();
        let m = x.getMonth()+1;
        let mm = m<10? '0'+m : m;
        let yyyy = x.getFullYear();
        return yyyy+'-'+mm+'-'+dd;
    }

    _showDateTimePicker = () => this.setState({ isDateTimePickerVisible: true });
    
      _hideDateTimePicker = () => this.setState({ isDateTimePickerVisible: false });
    
      _handleDatePicked = (date) => {
        //console.log('A date has been picked: ', date.toDateString());
         this.setState({
             dob: this._dateFormat(date.toISOString())
         })
        this._hideDateTimePicker();
      };

    register = () =>{
        if(this.state.name == '' || this.state.email == '' || this.state.dob == ''){
            this.refs.toast.show("Please fill all the details.");
            return;
        }
        if(this.state.password.length < 3){
            this.refs.toast.show("Please Enter minimum 4 character in a password.");
            return;
        }
        if(this.state.password != this.state.c_password){
            this.setState({
                c_password: ''
            })
            this.refs.toast.show("Password Mismatch");
            return;
        }
        else{
            this.setState({
                spinner: !this.state.spinner
            });
            fetch(URL+'/User_Registration', {
                method: 'POST',
                headers: {
                  Accept: 'application/json',
                  'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    name: this.state.name,
                    phone: this.state.phone,
                    email_id: this.state.email,
                    password: this.state.password,
                    dob: this.state.dob
                }),
              })
                .then(response => response.json())
                .then(responseJson => {
                    this.setState({
                        spinner: !this.state.spinner
                      });
                  if(responseJson.error){
                    this.refs.toast.show(responseJson.message);
                  }
                  else{
                    AsyncStorage.setItem('userToken', JSON.stringify(responseJson.data));
                    this.props.navigation.navigate('Home');
                  }
                  
                })
                .catch(error => {
                  //alert(JSON.stringify(error));
                  this.setState({
                    spinner: !this.state.spinner
                  });
                });
        }
    }

    logo(){
        const IPHONE7PLUS_WIDTH = 450;
        if (getWH.window.width <= IPHONE7PLUS_WIDTH) {
            return(
            <View style={styles.headlogo}>
                <Image source={require('../assets/images/ayushyaa.png')} style={{width: getWH.window.width - 60, height: 120}} />
            </View>
            )
        }
        else{
            return(
            <View style={styles.headlogo}>
                <Image source={require('../assets/images/ayushyaa.png')} style={{width: getWH.window.width - 60, height: 260}} />
            </View>
            )
        }
    }

    render(){
        return (
            <KeyboardAvoidingView behavior="padding" style={styles.container} enabled>
            <Spinner
                visible={this.state.spinner}
                cancelable={true}
                animation={"fade"}
                textContent={'Loading...'}
                textStyle={styles.spinnerTextStyle}
            />
            <View style={styles.container}>
            {this.logo()}
                <View>
                    <TextInput
                        style={styles.inputData}
                        placeholder="Name"
                        placeholderTextColor="#ccc"
                        underlineColorAndroid="transparent"
                        value={this.state.name}
                        onChangeText={text => this.setState({ name: text })}
                    />
                    <View style={styles.inputIcon}>
                        <Icon name="person-outline" size={25} color='#058f3a'/>
                    </View>
                </View>
                <View>
                    <TextInput
                        style={styles.inputData}
                        placeholder="Phone"
                        keyboardType="numeric"
                        editable={false}
                        placeholderTextColor="#ccc"
                        underlineColorAndroid="transparent"
                        value={this.state.phone}
                        maxLength={10}
                        onChangeText={text => this.setState({ phone: text })}
                    />
                    <View style={styles.inputIcon}>
                        <Icon name="phone" size={25} color='#058f3a'/>
                    </View>
                </View>
                <View>
                    <TextInput
                        style={styles.inputData}
                        placeholder="Email"
                        placeholderTextColor="#ccc"
                        underlineColorAndroid="transparent"
                        value={this.state.email}
                        keyboardType="email-address"
                        onChangeText={text => this.setState({ email: text })}
                    />
                    <View style={styles.inputIcon}>
                        <Icon name="email" size={25} color='#058f3a'/>
                    </View>
                </View>
                <View>
                <TouchableOpacity onPress={this._showDateTimePicker}
                activeOpacity={0.97}
                style={[{ zIndex: 999, elevation: 999, height:50, width: getWH.window.width - 60}]}
                >
                    <TextInput
                        pointerEvents="none"
                        style={styles.inputData}
                        placeholder="Date of Birth"
                        editable={false}
                        value={this.state.dob}
                        underlineColorAndroid="transparent"
                        onChangeText={text => this.setState({ dob: text })}
                    />
                </TouchableOpacity>
                <DateTimePicker
                    isVisible={this.state.isDateTimePickerVisible}
                    onConfirm={this._handleDatePicked}
                    onCancel={this._hideDateTimePicker}
                    datePickerModeAndroid={"spinner"}
                    />
                <View style={styles.inputIcon}>
                    <Icon name="date-range" size={25} color='#058f3a'/>
                </View>
                </View>
                <View>
                    <TextInput
                        style={styles.inputData}
                        placeholder="Confirm Password"
                        placeholderTextColor="#ccc"
                        underlineColorAndroid="transparent"
                        value={this.state.password}
                        secureTextEntry={true}
                        onChangeText={text => this.setState({ password: text })}
                    />
                    <View style={styles.inputIcon}>
                        <Icon name="vpn-key" size={25} color='#058f3a'/>
                    </View>
                </View>
                <View>
                    <TextInput
                        style={styles.inputData}
                        placeholder="Password"
                        placeholderTextColor="#ccc"
                        underlineColorAndroid="transparent"
                        value={this.state.c_password}
                        secureTextEntry={true}
                        onChangeText={text => this.setState({ c_password: text })}
                    />
                    <View style={styles.inputIcon}>
                        <Icon name="vpn-key" size={25} color='#058f3a'/>
                    </View>
                </View>
                <TouchableOpacity onPress={this.register.bind(this)}>
                    <View style={styles.loginbtn}>
                            <Text style={styles.loginbtnText}>Register</Text>
                    </View>
                </TouchableOpacity>
                <Toast ref="toast"
                positionValue = {10}
                />
            </View>
            </KeyboardAvoidingView>
        )
    }
}

const MyDrawerNavigator = createDrawerNavigator({
    GetOTP: {
      screen: GetOTP,
    },
    VerifyOTP: {
      screen: VerifyOTP,
    },
    Register: {
        screen: Register    }
  });
  
const RegisterScreen = createAppContainer(MyDrawerNavigator);

export default RegisterScreen