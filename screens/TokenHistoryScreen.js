import React from 'react'
import { View, Text, StyleSheet, ScrollView, AsyncStorage } from 'react-native'
import getWH from '../constants/Layout';
import URL from '../constants/Url';
import Spinner from 'react-native-loading-spinner-overlay';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: '#eeeeee',
    },
    displayContainer: {
        width : getWH.window.width - 30,
        height: 50,
        justifyContent: 'space-between',
        flexDirection: 'row',
        marginVertical: 10,
        backgroundColor: '#e0e0e0',
        borderLeftWidth: 6,
        borderColor: '#006600',
        padding: 8
    },
    spinnerTextStyle: {
        color: '#FFF'
    },
    errorMessage: {
        fontSize: 20,
         fontWeight: '500', 
         textAlign: 'center'
    }
})

class TokenHistoryScreen extends React.Component{
      constructor(props){
        super(props);
        this.state = {
            items : [],
            isLoaded: false,
            spinner: false
        }
        AsyncStorage.getItem("userToken")
        .then(value => {
            this.setState({
                 user_id: JSON.parse(value).user_id
            })
            this.getToken();
        })
        .done();
    }

    getToken = () =>{
        this.setState({
            spinner: !this.state.spinner
        });
        fetch(URL+'/getPastDetailsByPhone', {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
              user_id: this.state.user_id
          }),
        })  
          .then(response => response.json())
          .then(responseJson => {
              if(!responseJson.error){
                this.setState({
                    items: responseJson.data,
                    spinner: !this.state.spinner,
                })
              }
            else{
                this.setState({
                    spinner: !this.state.spinner,
                    isLoaded: true
                })
            }
        })
        .catch(error => {
            this.setState({
                spinner: !this.state.spinner
            });
        });
    }

    render(){
        var { isLoaded, items } = this.state;
        if (isLoaded) {
            return (
                <View style={[styles.container, {flex: 1, justifyContent: 'center'}]}>
                    <Text style={styles.errorMessage}>You Have Not Taken Tokens!</Text>
                </View>
            );
        }
        else{
        return (
                <View style={styles.container}>
                    <Spinner
                        visible={this.state.spinner}
                        cancelable={true}
                        animation={"fade"}
                        textContent={'Loading...'}
                        textStyle={styles.spinnerTextStyle}
                    />
                    <ScrollView>
                    <View>
                            {items.map((item, index) => (
                                 <View style={styles.displayContainer} key={index}>
                                        <View>
                                                <Text>Token ID: #{item.token_number}</Text>
                                                <Text>{item.date}</Text>
                                        </View>
                                        <View>
                                             <Text>{item.status}</Text>
                                            <Text>{item.start_time}</Text>
                                        </View>
                                </View>
                            ))}
                    </View>
                    </ScrollView>
                </View>
        )
        }
    }
}

export default TokenHistoryScreen