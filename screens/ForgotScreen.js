import React from 'react';
import {StyleSheet, View, TextInput, Text, TouchableOpacity, ToastAndroid, KeyboardAvoidingView, Image} from 'react-native';
import getWH from '../constants/Layout';
import themeColor from '../constants/Colors';
import URL from '../constants/Url';
import { Icon } from 'react-native-elements';
import { createAppContainer, createDrawerNavigator } from 'react-navigation';
import Spinner from 'react-native-loading-spinner-overlay';
import Toast from 'react-native-whc-toast';

const styles = StyleSheet.create({
    container: {
        backgroundColor: themeColor.AyushBackground,
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    headlogo: {
        marginVertical: 30,
        width: getWH.window.width - 50,
    },
    inputData: {
        width: getWH.window.width - 60,
        borderColor: themeColor.AyushInputBorderColor,
        borderWidth: 1,
        height: 40,
        color: themeColor.AyushInputColor,
        paddingHorizontal: 10,
        borderRadius: 5,
        marginVertical: 5 
    },
    inputIcon: {
        position: 'absolute',
        right: 5,
        top: 12,
    },
    loginbtn: {
        width: getWH.window.width - 60,
        borderColor: themeColor.AyushInputBorderColor,
        backgroundColor: themeColor.AyushButton,
        borderWidth: 1,
        height: 40,
        paddingHorizontal: 10,
        borderRadius: 5,
        marginVertical: 5,
        justifyContent: 'center',
        alignItems: 'center'
    },
    loginbtnText: {
        color: themeColor.AyushButtonText,
        alignItems: 'center',
        fontSize: 18,
        fontWeight: '500'
    },
    spinnerTextStyle: {
        color: '#FFF'
    },
})


class GetOTP extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            phone: "",
            spinner: false
        }
    }

    send_otp = () =>{
        this.setState({
            spinner: !this.state.spinner
        });
        fetch(URL+'/ForgotPassword', {
            method: 'POST',
            headers: {
              Accept: 'application/json',
              'Content-Type': 'application/json',
            },
            body: JSON.stringify({
              type: '1',
              phone: this.state.phone
            }),
          })
            .then(response => response.json())
            .then(responseJson => {
              if(responseJson.error){
                this.refs.toast.show(responseJson.message);
              }
              else{
                this.props.navigation.navigate('VerifyOTP', {'phone' : this.state.phone});
              }
              this.setState({
                spinner: !this.state.spinner
              });
            })
            .catch(error => {
                this.setState({
                    spinner: !this.state.spinner
                });
            });
    }

    logo(){
        const IPHONE7PLUS_WIDTH = 450;
        if (getWH.window.width <= IPHONE7PLUS_WIDTH) {
            return(
            <View style={styles.headlogo}>
                <Image source={require('../assets/images/ayushyaa.png')} style={{width: getWH.window.width - 60, height: 120}} />
            </View>
            )
        }
        else{
            return(
            <View style={styles.headlogo}>
                <Image source={require('../assets/images/ayushyaa.png')} style={{width: getWH.window.width - 60, height: 260}} />
            </View>
            )
        }
    }

    render(){
        return (
            <KeyboardAvoidingView behavior="padding" style={styles.container} enabled>
            <Spinner
                visible={this.state.spinner}
                cancelable={true}
                animation={"fade"}
                textContent={'Loading...'}
                textStyle={styles.spinnerTextStyle}
            />
            <View style={styles.container}>
            {this.logo()}
                <View>
                    <TextInput
                        style={styles.inputData}
                        placeholder="Phone"
                        keyboardType="numeric"
                        placeholderTextColor="#ccc"
                        underlineColorAndroid="transparent"
                        value={this.state.phone}
                        autoFocus={true}
                        maxLength={10}
                        onChangeText={text => this.setState({ phone: text })}
                    />
                    <View style={styles.inputIcon}>
                        <Icon name="phone" size={25} color='#058f3a'/>
                    </View>
                </View>
                <TouchableOpacity onPress={this.send_otp.bind(this)}>
                    <View style={styles.loginbtn}>
                            <Text style={styles.loginbtnText}>Get OTP</Text>
                    </View>
                </TouchableOpacity>
            </View>
            </KeyboardAvoidingView>
        )
    }
}

class VerifyOTP extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            phone: "",
            otp: "",
            spinner: false
        }
        const {state} = props.navigation;
        this.state.phone = state.params.phone;
    }

    verify_otp = () => {
        this.setState({
            spinner: !this.state.spinner
        });
        fetch(URL+'/VerifyOTPForgot', {
            method: 'POST',
            headers: {
              Accept: 'application/json',
              'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                phone: this.state.phone,
                otp: this.state.otp,
            }),
          })
            .then(response => response.json())
            .then(responseJson => {

              if(responseJson.error){
                this.refs.toast.show(responseJson.message);
              }
              else{
                    this.props.navigation.navigate('ChangePassword', {'phone': this.state.phone})
              }
              this.setState({
                spinner: !this.state.spinner
              });
            })
            .catch(error => {
              //alert(JSON.stringify(error));
              this.setState({
                spinner: !this.state.spinner
              });
            });
    }

    logo(){
        const IPHONE7PLUS_WIDTH = 450;
        if (getWH.window.width <= IPHONE7PLUS_WIDTH) {
            return(
            <View style={styles.headlogo}>
                <Image source={require('../assets/images/ayushyaa.png')} style={{width: getWH.window.width - 60, height: 120}} />
            </View>
            )
        }
        else{
            return(
            <View style={styles.headlogo}>
                <Image source={require('../assets/images/ayushyaa.png')} style={{width: getWH.window.width - 60, height: 260}} />
            </View>
            )
        }
    }

    

    render(){
        return (
            <KeyboardAvoidingView behavior="padding" style={styles.container} enabled>
            <Spinner
                visible={this.state.spinner}
                cancelable={true}
                animation={"fade"}
                textContent={'Loading...'}
                textStyle={styles.spinnerTextStyle}
            />
            <View style={styles.container}>
            {this.logo()}
                <View>
                    <TextInput
                        style={styles.inputData}
                        placeholder="Phone"
                        keyboardType="numeric"
                        placeholderTextColor="#ccc"
                        underlineColorAndroid="transparent"
                        value={this.state.phone}
                        maxLength={10}
                        editable = {false}
                        onChangeText={text => this.setState({ phone: text })}
                    />
                    <View style={styles.inputIcon}>
                        <Icon name="phone" size={25} color='#058f3a'/>
                    </View>
                </View>
                <View>
                    <TextInput
                        style={styles.inputData}
                        placeholder="Enter OTP"
                        keyboardType="numeric"
                        placeholderTextColor="#ccc"
                        underlineColorAndroid="transparent"
                        maxLength={4}
                        value={this.state.otp}
                        onChangeText={text => this.setState({ otp: text })}
                    />
                    
                    <View style={styles.inputIcon}>
                        <Icon name="phone" size={25} color='#058f3a'/>
                    </View>
                </View>
                <TouchableOpacity onPress={this.verify_otp.bind(this)}>
                    <View style={styles.loginbtn}>
                            <Text style={styles.loginbtnText}>Verify OTP</Text>
                    </View>
                </TouchableOpacity>
                <Toast ref="toast"
                positionValue = {10}
                />
            </View>
            </KeyboardAvoidingView>
        )
    }
}

class ChangePassword extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            phone: "",
            password: "",
            conf_password: "",
            spinner: false
        }
        const {state} = props.navigation;
        this.state.phone = state.params.phone;
    }

    change_password = () =>{
        if(this.state.password == "" || this.state.conf_password == ""){
            this.refs.toast.show("Enter Valid Password");
        }
        else if(this.state.password != this.state.conf_password){
            this.setState({
                conf_password: ""
            })
            this.refs.toast.show("Password Not Matching");
        }
        else{
            this.setState({
                spinner: !this.state.spinner
              });
            fetch(URL+'/ChangePassword', {
                method: 'POST',
                headers: {
                  Accept: 'application/json',
                  'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    phone: this.state.phone,
                    password: this.state.password,
                    con_password: this.state.conf_password,
                }),
              })
                .then(response => response.json())
                .then(responseJson => {
                  if(responseJson.error){
                    this.refs.toast.show(responseJson.message);
                  }else{
                    this.props.navigation.navigate('Login');
                  }
                  this.setState({
                    spinner: !this.state.spinner
                  });
                })
                .catch(error => {
                  //alert(JSON.stringify(error));
                  this.setState({
                    spinner: !this.state.spinner
                  });
                });
        }
    }

    logo(){
        const IPHONE7PLUS_WIDTH = 450;
        if (getWH.window.width <= IPHONE7PLUS_WIDTH) {
            return(
            <View style={styles.headlogo}>
                <Image source={require('../assets/images/ayushyaa.png')} style={{width: getWH.window.width - 60, height: 120}} />
            </View>
            )
        }
        else{
            return(
            <View style={styles.headlogo}>
                <Image source={require('../assets/images/ayushyaa.png')} style={{width: getWH.window.width - 60, height: 260}} />
            </View>
            )
        }
    }

    render(){
        return (
            <KeyboardAvoidingView behavior="padding" style={styles.container} enabled>
            <Spinner
                visible={this.state.spinner}
                cancelable={true}
                animation={"fade"}
                textContent={'Loading...'}
                textStyle={styles.spinnerTextStyle}
            />
            <View style={styles.container}>
            {this.logo()}
                <View>
                    <TextInput
                        style={styles.inputData}
                        placeholder="Phone"
                        keyboardType="numeric"
                        placeholderTextColor="#ccc"
                        underlineColorAndroid="transparent"
                        value={this.state.phone}
                        maxLength={10}
                        editable = {false}
                        onChangeText={text => this.setState({ phone: text })}
                    />
                    <View style={styles.inputIcon}>
                        <Icon name="phone" size={25} color='#058f3a'/>
                    </View>
                </View>
                <View>
                    <TextInput
                        style={styles.inputData}
                        placeholder="Password"
                        placeholderTextColor="#ccc"
                        underlineColorAndroid="transparent"
                        value={this.state.password}
                        secureTextEntry={true}
                        onChangeText={text => this.setState({ password: text })}
                    />
                    <View style={styles.inputIcon}>
                        <Icon name="vpn-key" size={25} color='#058f3a'/>
                    </View>
                </View>
                <View>
                    <TextInput
                        style={styles.inputData}
                        placeholder="Confirm Password"
                        placeholderTextColor="#ccc"
                        underlineColorAndroid="transparent"
                        value={this.state.conf_password}
                        secureTextEntry={true}
                        onChangeText={text => this.setState({ conf_password: text })}
                    />
                    <View style={styles.inputIcon}>
                        <Icon name="vpn-key" size={25} color='#058f3a'/>
                    </View>
                </View>
                <TouchableOpacity onPress={this.change_password.bind(this)}>
                    <View style={styles.loginbtn}>
                            <Text style={styles.loginbtnText}>Submit</Text>
                    </View>
                </TouchableOpacity>
            </View>
            </KeyboardAvoidingView>
        )
    }
}

const MyDrawerNavigator = createDrawerNavigator({
    GetOTP: {
      screen: GetOTP,
    },
    VerifyOTP: {
      screen: VerifyOTP,
    },
    ChangePassword: {
        screen: ChangePassword
    }
  });
  
  const ForgotScreen = createAppContainer(MyDrawerNavigator);

export default ForgotScreen