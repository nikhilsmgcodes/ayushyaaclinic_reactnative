import React from 'react';
import { View, Text, TouchableHighlight, StyleSheet, AsyncStorage } from 'react-native';
import { Icon } from 'react-native-elements';

const styles = StyleSheet.create({
      contentContainer: {
        flex: 5
      },
      outerblock: {
        height: 60,
        padding: 8,
        flexDirection: 'row',
        borderBottomWidth: 1,
        borderColor: '#ccc',
        backgroundColor: 'transparent',
        borderRadius: 6,
        justifyContent: 'space-between',
        alignItems: 'center'
      },
})

export default class SettingsScreen extends React.Component {
  static navigationOptions = {
    title: 'Ayushyaa Clinic',
    headerStyle: {
      backgroundColor: '#006600',
    },
    headerTintColor: '#ffffff',
    headerTitleStyle: {
      fontWeight: 'bold',
      flex: 1,
      textAlign: 'center',
      color: "#fff",
      textAlign: 'center',
      textShadowColor: 'rgba(0, 0, 0, 0.75)',
      textShadowOffset: {width: -1, height: 1},
      textShadowRadius: 10,
      fontSize: 24,
    },
  }

  navigateToProfile = () =>{
    this.props.navigation.navigate('Profile');
  }

  navigateToHistory = () =>{
    this.props.navigation.navigate('History');
  }

  navigateToChangePassword = () =>{
    this.props.navigation.navigate('ChangePassword');
  }

  navigateToLogout = async () =>{
     await AsyncStorage.clear();
     this.props.navigation.navigate('Auth');
  }

  constructor(props){
    super(props);
    this.state = {
      items: [{icon: 'person', name: 'My Account', link: this.navigateToProfile.bind(this)},
              {icon: 'history', name: 'My Tokens', link: this.navigateToHistory.bind(this)}, 
              {icon: 'vpn-key', name: 'Change Password', link: this.navigateToChangePassword.bind(this)},
              {icon: 'exit-to-app', name: 'Log Out',  link: this.navigateToLogout.bind(this)}]
      }
  }

  render() {
    return (
      <View style={styles.contentContainer}>
           {this.state.items.map((item, index) => (
                  <TouchableHighlight key={index}
                      onPress = {item.link}
                      underlayColor = '#006600'
                      >
                    <View style={styles.outerblock}>
                        <View style={{flexDirection: 'row', alignItems: 'center'}}>
                          <View>
                             <Icon name={item.icon} size={28} 
                                color="#000"
                                />
                          </View>
                          <View style={{marginLeft: 10}}>
                              <Text style={{fontSize: 16}}>{item.name}</Text>
                          </View>
                        </View>
                        <View style={{alignItems: 'center', paddingTop: 4}}>
                            <Icon name="keyboard-arrow-right" size={28} color='#006600'/>
                        </View>
                    </View>
                  </TouchableHighlight>
              ))}
           </View>
    )
  }
}
