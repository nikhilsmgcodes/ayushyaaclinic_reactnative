import React from "react";
import {
  StyleSheet,
  View,
  Text,
  TouchableHighlight,
  TouchableOpacity,
  Image,
  Platform
} from "react-native";
import ImageSlider from "react-native-image-slider";
import { Icon } from "react-native-elements";
import getWH from "../constants/Layout";

const styles = StyleSheet.create({
  container: {
    backgroundColor: "#006600",
    flex: 1,
    paddingTop: 24
  },
  sliderContainer: {
    flex: 3,
    zIndex: 2
  },
  logoContainer: {
    position: "absolute",
    bottom: -25,
    left: 18,
    zIndex: 2
  },
  logoImage: {
    width: 80,
    height: 80,
    borderRadius: 40
  },
  taketoken: {
    flex: 1,
    justifyContent: "space-around",
    opacity: 0.8,
    backgroundColor: "#006600",
    borderColor: "#013300",
    borderWidth: 2,
    zIndex: -1
  },
  tokenText: {
    fontSize: 24,
    color: "#fff",
    textAlign: "center",
    textShadowColor: "rgba(0, 0, 0, 0.75)",
    textShadowOffset: { width: -1, height: 1 },
    textShadowRadius: 10
  },
  contentContainer: {
    flex: 5,
    backgroundColor: "#eeeeee"
  },
  outerblock: {
    height: 75,
    padding: 10,
    flexDirection: "row",
    borderBottomWidth: 1,
    borderColor: "#ccc",
    backgroundColor: "transparent",
    borderRadius: 6,
    justifyContent: "space-between",
    alignItems: "center"
  },
  imageOuter: {
    width: 60,
    height: 60,
    //backgroundColor: '#006600',
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 50
  },
  containerImage: {
    width: 40,
    height: 40,
    borderRadius: 20
  },
  tokenTextContainer: {
    flexDirection: "column",
    flex: 5,
    backgroundColor: "#eeeeee",
    alignItems: "center",
    justifyContent: "center"
  },
  circleIphone7: {
    //flex: 1,
    width: 180,
    height: 180,
    borderRadius: 200,
    backgroundColor: "#006600",
    alignItems: "center",
    justifyContent: "center",
    borderColor: "#009900",
    borderWidth: 4
  },
  circleIpad:{
    width: 400,
    height: 400,
    borderRadius: 250,
    backgroundColor: "#006600",
    alignItems: "center",
    justifyContent: "center",
    borderColor: "#009900",
    borderWidth: 10
  },
  clicleTokenTextIphone: {
    fontSize: 26,
    color: "#fff",
    textAlign: "center",
    textShadowColor: "rgba(0, 0, 0, 0.75)",
    textShadowOffset: { width: 1, height: 4 },
    textShadowRadius: 10
  },
  clicleTokenTextIpad: {
    fontSize: 46,
    color: "#fff",
    textAlign: "center",
    textShadowColor: "rgba(0, 0, 0, 0.75)",
    textShadowOffset: { width: 1, height: 4 },
    textShadowRadius: 10
  },
  androidContainer: {
    flex: 6,
    backgroundColor: "#eeeeee"
  },
  iosContainer: {
    flex: 5,
    backgroundColor: "#eeeeee"
  },
  doctorServiceContainer: {
    flexDirection: "row"
  },
  doctorContainer: {
    justifyContent: "flex-start"
    // alignItems: 'flex-start',
    // alignContent: 'flex-start'
  }
});

export default class HomeScreen extends React.Component {
  static navigationOptions = {
    header: null
  };
  constructor(props) {
    super(props);
    this.state = {
      items: [
        {
          icon: "local-hospital",
          name: "Doctors",
          link: this.navigateToWebDoctors.bind(this),
          image: require("../assets/images/Icon_1.png")
        },
        {
          icon: "vpn-key",
          name: "About Us",
          link: this.navigateToWebAbout.bind(this),
          image: require("../assets/images/Icon_2.png")
        },
        {
          icon: "settings",
          name: "Services",
          link: this.navigateToWebServices.bind(this),
          image: require("../assets/images/Icon_3.png")
        },
        {
          icon: "contactt-phone",
          name: "Contact Us",
          link: this.navigateToWebContactUs.bind(this),
          image: require("../assets/images/Icon_4.png")
        }
      ]
    };
  }

  navigateToWebAbout = () => {
    this.props.navigation.navigate("About");
  };
  navigateToWebDoctors = () => {
    this.props.navigation.navigate("Doctor");
  };
  navigateToWebServices = () => {
    this.props.navigation.navigate("Service");
  };
  navigateToWebContactUs = () => {
    this.props.navigation.navigate("Contact");
  };
  navigateToToken = () => {
    this.props.navigation.navigate("Token");
  };

  homeData() {
    // if (Platform.OS === "android") {
    //   return (
    //     <View style={styles.androidContainer}>
    //       <View style={styles.taketoken}>
    //         <TouchableOpacity onPress={this.navigateToToken.bind(this)}>
    //           <Text style={styles.tokenText}>TAKE TOKEN</Text>
    //         </TouchableOpacity>
    //       </View>
    //       <View style={styles.contentContainer}>
    //         {this.state.items.map((item, index) => (
    //           <TouchableHighlight
    //             key={index}
    //             onPress={item.link}
    //             underlayColor="#eeeeee"
    //           >
    //             <View style={styles.outerblock}>
    //               <View style={{ flexDirection: "row", alignItems: "center" }}>
    //                 <View style={styles.imageOuter}>
    //                   <Image
    //                     style={styles.containerImage}
    //                     source={item.image}
    //                   />
    //                 </View>
    //                 <View>
    //                   <Text style={{ fontSize: 18, textAlign: "center" }}>
    //                     {item.name}
    //                   </Text>
    //                 </View>
    //               </View>
    //               <View style={{ alignItems: "center", paddingTop: 6 }}>
    //                 <Icon
    //                   name="keyboard-arrow-right"
    //                   size={28}
    //                   color="#006600"
    //                 />
    //               </View>
    //             </View>
    //           </TouchableHighlight>
    //         ))}
    //       </View>
    //     </View>
    //   );
    // } else {
      return (
        <View style={styles.iosContainer}>
          <View style={styles.taketoken}>
            <Text style={styles.tokenText}>TOKEN SYSTEM</Text>
          </View>
          <View style={styles.tokenTextContainer}>
            <TouchableHighlight
              onPress={this.navigateToToken.bind(this)}
              underlayColor="#eeeeee"
            >
              {this.clickHereButton()}
            </TouchableHighlight>
            <TouchableHighlight
              onPress={this.navigateToWebDoctors.bind(this)}
              underlayColor="#eeeeee"
              style={{ position: "absolute", top: 20, left: 20 }}
            >
              <View
                style={{
                  flex: 1,
                  flexDirection: "row",
                  alignItems: "flex-end",
                  justifyContent: "space-between"
                }}
                //onPress={this.state.items[0].link}
              >
                <View>
                  <Image
                    style={styles.containerImage}
                    source={require("../assets/images/Icon_1.png")}
                  />
                </View>
                <View>
                  <Text style={{ fontSize: 16, color: "#006600" }}>
                    {" "}
                    Doctors
                  </Text>
                </View>
              </View>
            </TouchableHighlight>
            <TouchableHighlight
              onPress={this.navigateToWebServices.bind(this)}
              underlayColor="#eeeeee"
              style={{ position: "absolute", top: 20, right: 20 }}
            >
              <View
                style={{
                  flex: 1,
                  flexDirection: "row",
                  alignItems: "flex-end",
                  justifyContent: "space-between"
                }}
              >
                <View>
                  <Text style={{ fontSize: 16, color: "#006600" }}>
                    Services{" "}
                  </Text>
                </View>
                <View>
                  <Image
                    style={styles.containerImage}
                    source={require("../assets/images/Icon_3.png")}
                  />
                </View>
              </View>
            </TouchableHighlight>
            <TouchableHighlight
              onPress={this.navigateToWebAbout.bind(this)}
              underlayColor="#eeeeee"
              style={{ position: "absolute", bottom: 20, left: 20 }}
            >
              <View
                style={{
                  flex: 1,
                  flexDirection: "row",
                  alignItems: "flex-start",
                  justifyContent: "space-between"
                }}
              >
                <View>
                  <Image
                    style={styles.containerImage}
                    source={require("../assets/images/Icon_2.png")}
                  />
                </View>
                <View>
                  <Text style={{ fontSize: 16, color: "#006600" }}>
                    {" "}
                    About Us
                  </Text>
                </View>
              </View>
            </TouchableHighlight>
            <TouchableHighlight
              onPress={this.navigateToWebContactUs.bind(this)}
              underlayColor="#eeeeee"
              style={{ position: "absolute", bottom: 20, right: 20 }}
            >
              <View
                style={{
                  flex: 1,
                  flexDirection: "row",
                  alignItems: "flex-start",
                  justifyContent: "space-between"
                }}
              >
                <View>
                  <Text style={{ fontSize: 16, color: "#006600" }}>
                    Contact Us{" "}
                  </Text>
                </View>
                <View>
                  <Image
                    style={styles.containerImage}
                    source={require("../assets/images/Icon_4.png")}
                  />
                </View>
              </View>
            </TouchableHighlight>
          </View>
        </View>
      );
    // }
  }

  clickHereButton() {
    const IPHONE7PLUS_WIDTH = 450;
    if (getWH.window.width <= IPHONE7PLUS_WIDTH) {
      return (
        <View style={styles.circleIphone7}>
          <Text style={styles.clicleTokenTextIphone}>CLICK HERE</Text>
        </View>
      );
    } else {
      return (
        <View style={styles.circleIpad}>
          <Text style={styles.clicleTokenTextIpad}>CLICK HERE</Text>
        </View>
      );
    }
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.sliderContainer}>
          <ImageSlider
            autoPlayWithInterval={2000}
            thumbTintColor="#000"
            images={[
              require("../assets/images/slider1.png"),
              require("../assets/images/slider2.png"),
              require("../assets/images/slider3.png")
            ]}
          />
          <View style={styles.logoContainer}>
            <Image
              style={styles.logoImage}
              source={require("../assets/images/logo.png")}
            />
          </View>
        </View>
        {this.homeData()}
      </View>
    );
  }
}
