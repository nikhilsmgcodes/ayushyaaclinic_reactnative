const tintColor = '#ffffff';

export default {
  AyushBackground: '#fff',
  AyushInputBorderColor : '#058f3a',
  AyushInputColor : '#000',
  AyushButton: '#058f3a',
  AyushButtonText: '#fff',
  tintColor,
  tabIconDefault: '#ccc',
  tabIconSelected: tintColor,
  tabBar: '#fefefe',
  errorBackground: 'red',
  errorText: '#fff',
  warningBackground: '#EAEB5E',
  warningText: '#666804',
  noticeBackground: tintColor,
  noticeText: '#fff',
};
