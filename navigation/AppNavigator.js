import { createAppContainer, createStackNavigator, createSwitchNavigator } from 'react-navigation';

import MainTabNavigator from './MainTabNavigator';
import LoginScreen from '../screens/LoginScreen';
import ForgotScreen from '../screens/ForgotScreen';
import InitialScreen  from '../screens/InitialScreen';
import RegisterScreen from '../screens/RegisterScreen';
import DoctorScreen from '../screens/DoctorScreen';
import AboutSreen from '../screens/AboutScreen';
import ServiceScreen from '../screens/ServiceScreen';
import ContactScreen from '../screens/ContactScreen';
import TokenScreen from '../screens/TokenScreen';
import ProfileScreen from '../screens/ProfileScreen';
import TokenHistoryScreen from '../screens/TokenHistoryScreen';
import ChangePasswordScreen from '../screens/ChangePasswordScreen';
import AuthLoadingScreen from '../screens/AuthLoadingScreen';

const AuthStack = createStackNavigator({
    Initial: {
      screen: InitialScreen,
      navigationOptions: {
        header: null
      }
    },
    Login: {
      screen: LoginScreen,
      navigationOptions: {
        header: null
      }
    },
    Register: {
      screen: RegisterScreen,
      navigationOptions: {
        header: null
      }
    },
    Forgot: {
      screen: ForgotScreen,
      navigationOptions: {
        header: null
      }
    }
  },
    {
        initialRouteName: 'Initial'
});

const AppStack = createStackNavigator({
  // You could add another route here for authentication.
  // Read more at https://reactnavigation.org/docs/en/auth-flow.html
    
    Main: {
      screen: MainTabNavigator,
      navigationOptions: {
        header: null
      }
    },
    Doctor: {
      screen: DoctorScreen,
      navigationOptions: {
        title: "Doctor"
      }
    },
    About: {
      screen: AboutSreen,
      navigationOptions: {
        title: "About"
      }
    },
    Service: {
      screen: ServiceScreen,
      navigationOptions: {
        title: "Service"
      }
    },
    Contact: {
      screen: ContactScreen,
      navigationOptions: {
        title: "Contact"
      }
    },
    Token: {
      screen: TokenScreen,
      navigationOptions: {
        title: "Token"
      }
    },
    Profile: {
      screen: ProfileScreen,
      navigationOptions: {
        title: "My Profile"
      }
    },
    History: {
      screen: TokenHistoryScreen,
      navigationOptions: {
        title: "Token History"
      }
    },
    ChangePassword: {
      screen: ChangePasswordScreen,
      navigationOptions: {
        title: "Password Update"
      }
    }
  },
  {
    initialRouteName: 'Main',
    defaultNavigationOptions: {
      headerStyle: {
        backgroundColor: '#006600',
      },
      headerTintColor: '#ffffff',
      headerTitleStyle: {
        fontWeight: 'bold',
      },
    }
});

export default createAppContainer(createSwitchNavigator(
  {
    AuthLoading: AuthLoadingScreen,
    App: AppStack,
    Auth: AuthStack,
  },
  {
    initialRouteName: 'AuthLoading',
  }
));