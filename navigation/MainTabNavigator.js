import React from 'react';
import { Platform } from 'react-native';
import { createStackNavigator, createBottomTabNavigator } from 'react-navigation';

import TabBarIcon from '../components/TabBarIcon';
import HomeScreen from '../screens/HomeScreen';
import ShowtokenScreen from '../screens/ShowtokenScreen';
import SettingsScreen from '../screens/SettingsScreen';

const HomeStack = createStackNavigator({
  Home: HomeScreen,
});

HomeStack.navigationOptions = {
  tabBarLabel: 'Home',
  tabBarOptions: {
    activeBackgroundColor: '#013300',
    inactiveBackgroundColor : '#006600',
    activeTintColor: '#fff',
    inactiveTintColor: '#fff',
  },
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name={
        Platform.OS === 'ios'
          ? `ios-home${focused ? '' : ''}`
          : 'md-home'
      }
    />
  ),
};

const LinksStack = createStackNavigator({
  Links: ShowtokenScreen,
});

LinksStack.navigationOptions = {
  tabBarLabel: 'Token',
  tabBarOptions: {
    activeBackgroundColor: '#013300',
    inactiveBackgroundColor : '#006600',
    activeTintColor: '#fff',
    inactiveTintColor: '#fff',
  },
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name={Platform.OS === 'ios' ? 'ios-calendar' : 'md-calendar'}
    />
  ),
};

const SettingsStack = createStackNavigator({
  Settings: SettingsScreen,
});

SettingsStack.navigationOptions = {
  tabBarLabel: 'Profile',
  tabBarOptions: {
    activeBackgroundColor: '#013300',
    inactiveBackgroundColor : '#006600',
    activeTintColor: '#fff',
    inactiveTintColor: '#fff',
  },
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name={Platform.OS === 'ios' ? 'ios-person' : 'md-person'}
    />
  ),
};

export default createBottomTabNavigator({
  HomeStack,
  LinksStack,
  SettingsStack,
});
